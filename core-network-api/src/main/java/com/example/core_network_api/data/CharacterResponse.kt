package com.example.core_network_api.data

data class CharacterResponse(
    val id: Int,
    val name: String,
    val gender: String,
    val status: String,
    val species: String,
    val created: String,
    val image: String
)
