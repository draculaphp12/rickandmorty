package com.example.core_network_api.data

interface HttpClientApi {
    suspend fun getCharacters(page: Int = 1): List<CharacterResponse>

    suspend fun searchCharacters(name: String): List<CharacterResponse>

    suspend fun getCharacter(characterId: Int): CharacterResponse
}