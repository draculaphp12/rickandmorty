package com.example.core_network_impl.data

import com.example.core_network_api.data.CharacterResponse
import com.example.core_network_api.data.HttpClientApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject

class HttpClientImpl @Inject constructor() : HttpClientApi {
    companion object RetrofitClient {
        private const val BASE_URL = "https://rickandmortyapi.com/api/"

        private var retrofit: Retrofit? = null

        private var gson: Gson = GsonBuilder()
            .setLenient()
            .create()

        private fun getClient(): Retrofit {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()
            }
            return retrofit!!
        }

        val rickAndMortyService: RickAndMortyService
            get() = getClient().create(RickAndMortyService::class.java)
    }

    override suspend fun getCharacters(page: Int): List<CharacterResponse> {
        return rickAndMortyService.getCharactersAsync(page).await().results
    }

    override suspend fun searchCharacters(name: String): List<CharacterResponse> {
        return rickAndMortyService.searchCharactersAsync(name).await().results
    }

    override suspend fun getCharacter(characterId: Int): CharacterResponse {
        return rickAndMortyService.getCharacterAsync(characterId).await()
    }
}

interface RickAndMortyService {
    @GET("character")
    @Headers("Content-Type: application/json")
    fun getCharactersAsync(@Query("page") page: Int = 1): Deferred<CharactersListResponse>

    @GET("character")
    @Headers("Content-Type: application/json")
    fun searchCharactersAsync(@Query("name") name: String): Deferred<CharactersListResponse>

    @GET("character/{characterId}")
    @Headers("Content-Type: application/json")
    fun getCharacterAsync(@Path("characterId") characterId: Int): Deferred<CharacterResponse>
}
