package com.example.core_network_impl.data

import com.example.core_network_api.data.CharacterResponse

data class CharactersListResponse(
    val results: List<CharacterResponse>
)
