package com.example.core_db_impl.di

import com.example.core_db_api.di.CoreDbApi
import dagger.Component
import javax.inject.Singleton

@Component(modules = [DbModule::class])
@Singleton
abstract class CoreDbComponent : CoreDbApi {
    companion object {
        @Volatile
        private var coreNetworkComponent: CoreDbComponent? = null

        fun get(): CoreDbComponent {
            if (coreNetworkComponent == null) {
                synchronized(CoreDbComponent::class.java) {
                    if (coreNetworkComponent == null) {
                        coreNetworkComponent = DaggerCoreDbComponent.builder().build()
                    }
                }
            }
            return coreNetworkComponent!!
        }
    }
}