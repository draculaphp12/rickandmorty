package com.example.core_db_impl.data

import android.content.Context
import androidx.room.Room
import com.example.core_db_api.data.DbClientApi
import com.example.core_db_impl.data.repositories.CharactersRepositoryImpl
import javax.inject.Inject

class DbClientImpl @Inject constructor() : DbClientApi {
    private lateinit var applicationContext: Context

    private val database: AppDatabase by lazy {
        Room.databaseBuilder(applicationContext, AppDatabase::class.java, "RickAndMorty")
            .build()
    }

    override val charactersRepository: CharactersRepositoryImpl by lazy {
        CharactersRepositoryImpl(database.getCharactersDao())
    }

    override fun init(context: Context) {
        applicationContext = context
    }
}