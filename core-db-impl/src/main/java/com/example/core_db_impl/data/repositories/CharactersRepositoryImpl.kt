package com.example.core_db_impl.data.repositories

import com.example.core_db_api.data.repositories.CharactersRepository
import com.example.core_db_impl.data.dao.CharactersDao
import com.example.core_db_api.data.models.CharacterDb
import com.example.core_db_impl.data.entities.toCharacterEntity

class CharactersRepositoryImpl(
    private val charactersDao: CharactersDao
) : CharactersRepository {
    override suspend fun getById(characterId: Int): CharacterDb? {
        return charactersDao.getCharacterById(characterId)?.toCharacter()
    }

    override suspend fun getAll(limit: Int, offset: Int, searchBy: String): List<CharacterDb>? {
        return charactersDao.getAllCharacters(limit, offset, searchBy)?.map {
            it.toCharacter()
        }
    }

    override suspend fun saveAll(characters: List<CharacterDb>) {
        charactersDao.saveCharacters(characters.map { it.toCharacterEntity() })
    }
}