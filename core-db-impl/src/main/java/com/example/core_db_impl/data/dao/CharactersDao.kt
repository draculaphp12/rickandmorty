package com.example.core_db_impl.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.core_db_impl.data.entities.CharacterEntity

@Dao
interface CharactersDao {
    @Query("SELECT * FROM characters WHERE id = :characterId")
    suspend fun getCharacterById(characterId: Int): CharacterEntity?

    @Query("SELECT * FROM characters " +
            "WHERE :searchBy = '' OR name LIKE '%' || :searchBy || '%' " + // search substring
            "LIMIT :limit OFFSET :offset") // return max :limit number of users starting from :offset position
    suspend fun getAllCharacters(limit: Int, offset: Int, searchBy: String = ""): List<CharacterEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveCharacters(characters: List<CharacterEntity>)
}