package com.example.core_db_impl.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.core_db_api.data.models.CharacterDb

@Entity(
    tableName = "characters"
)
data class CharacterEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val name: String,
    val gender: String,
    val status: String,
    val species: String,
    val created: String,
    val image: String
) {
    fun toCharacter(): CharacterDb = CharacterDb(
        id = id,
        name = name,
        gender = gender,
        status = status,
        species = species,
        created = created,
        image = image
    )
}

fun CharacterDb.toCharacterEntity(): CharacterEntity = CharacterEntity(
    id, name, gender, status, species, created, image
)
