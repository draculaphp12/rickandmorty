package com.example.core_db_impl.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.core_db_impl.data.dao.CharactersDao
import com.example.core_db_impl.data.entities.CharacterEntity

@Database(
    version = 1,
    entities = [
        CharacterEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCharactersDao(): CharactersDao
}