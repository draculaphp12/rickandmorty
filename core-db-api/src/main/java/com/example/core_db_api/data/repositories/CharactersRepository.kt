package com.example.core_db_api.data.repositories

import com.example.core_db_api.data.models.CharacterDb


interface CharactersRepository {
    suspend fun getById(characterId: Int): CharacterDb?

    suspend fun getAll(limit: Int, offset: Int, searchBy: String = ""): List<CharacterDb>?

    suspend fun saveAll(characters: List<CharacterDb>)
}