package com.example.core_db_api.data.models

data class CharacterDb(
    val id: Int,
    val name: String,
    val gender: String,
    val status: String,
    val species: String,
    val created: String,
    val image: String
)
