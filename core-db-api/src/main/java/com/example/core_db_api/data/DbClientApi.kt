package com.example.core_db_api.data

import android.content.Context
import com.example.core_db_api.data.repositories.CharactersRepository

interface DbClientApi {
    val charactersRepository: CharactersRepository

    fun init(context: Context)
}