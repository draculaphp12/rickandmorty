package com.example.core_navigation

sealed class NavigationFlow {
    object CharactersListFlow : NavigationFlow()

    object CharacterDetailFlow : NavigationFlow()
}