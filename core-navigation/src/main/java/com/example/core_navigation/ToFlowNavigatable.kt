package com.example.core_navigation

import android.net.Uri


interface ToFlowNavigatable {
    fun navigateToFlow(flow: NavigationFlow)

    fun navigateToLink(uri: Uri)
}