package com.example.core_navigation

import android.net.Uri
import android.os.Bundle
import androidx.navigation.NavController

class Navigator {
    lateinit var navController: NavController

    fun navigateToFlow(navigationFlow: NavigationFlow) = when (navigationFlow) {
        NavigationFlow.CharactersListFlow -> navController.navigate(MainNavGraphDirections.actionGlobalCharactersListFlow())
        is NavigationFlow.CharacterDetailFlow -> navController.navigate(MainNavGraphDirections.actionGlobalCharacterDetailFlow())
    }
    
    fun navigateToLink(uri: Uri) {
        navController.navigate(uri)
    }
}