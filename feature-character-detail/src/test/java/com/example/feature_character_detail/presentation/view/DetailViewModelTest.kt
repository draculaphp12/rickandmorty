package com.example.feature_character_detail.presentation.view

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import com.example.feature_character_detail.domain.model.Character
import com.example.feature_character_detail.domain.usecase.GetCharacterUseCase
import com.example.feature_character_detail.domain.usecase.LoadCharacterFromDbUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.reset

@ExperimentalCoroutinesApi
class DetailViewModelTest {
    private val getCharacterUseCase = mock<GetCharacterUseCase>()
    private val loadCharacterUseCase = mock<LoadCharacterFromDbUseCase>()
    private lateinit var detailViewModel: DetailViewModel
    private val testCharacter = Character(
        1,
        "Rick Sanchez",
        "male",
        "alive",
        "human",
        "2022-01-06",
        ""
    )

    @AfterEach
    fun afterEach() {
        reset(getCharacterUseCase)
        reset(loadCharacterUseCase)
        ArchTaskExecutor.getInstance().setDelegate(null)
        Dispatchers.resetMain()
    }

    @BeforeEach
    fun beforeEach() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
            override fun executeOnDiskIO(runnable: Runnable) {
                runnable.run()
            }

            override fun postToMainThread(runnable: Runnable) {
                runnable.run()
            }

            override fun isMainThread(): Boolean {
                return true
            }

        })
        detailViewModel = DetailViewModel(
            getCharacterUseCase,
            loadCharacterUseCase
        )
    }

    @Test
    fun `should return character from network`() = runBlocking {
        Mockito.`when`(getCharacterUseCase.execute(any())).thenReturn(testCharacter)
        detailViewModel.getCharacter(testCharacter.id)
        val actual = detailViewModel.character.value

        Mockito.verify(getCharacterUseCase, Mockito.times(1))
            .execute(testCharacter.id)
        Assertions.assertEquals(testCharacter, actual)
    }

    @Test
    fun `should return character from db`() = runBlocking {
        Mockito.`when`(getCharacterUseCase.execute(any())).thenAnswer { throw Exception() }
        Mockito.`when`(loadCharacterUseCase.execute(any())).thenReturn(testCharacter)
        detailViewModel.getCharacter(testCharacter.id)
        val actual = detailViewModel.character.value

        Mockito.verify(loadCharacterUseCase, Mockito.times(1))
            .execute(testCharacter.id)
        Assertions.assertEquals(testCharacter, actual)
    }
}