package com.example.feature_character_detail.domain.usecase

import com.example.feature_character_detail.domain.CharacterRepository
import com.example.feature_character_detail.domain.model.Character
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.mock

class GetCharacterUseCaseTest {
    private val charactersRepository = mock<CharacterRepository>()
    private val testCharacter = Character(
        1,
        "Rick Sanchez",
        "male",
        "alive",
        "human",
        "2022-01-06",
        ""
    )

    @Test
    fun `should return the same character as in repository`() = runBlocking {
        val useCase = GetCharacterUseCase(characterRepository = charactersRepository)
        Mockito.`when`(charactersRepository.getCharacter(any())).thenReturn(testCharacter)
        val actual = useCase.execute(testCharacter.id)
        Assertions.assertEquals(testCharacter, actual)
    }
}