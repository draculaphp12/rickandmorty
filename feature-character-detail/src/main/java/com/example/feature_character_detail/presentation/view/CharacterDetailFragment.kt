package com.example.feature_character_detail.presentation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.core_navigation.NavigationFlow
import com.example.core_navigation.ToFlowNavigatable
import com.example.feature_character_detail.R
import com.example.feature_character_detail.databinding.FragmentCharacterDetailBinding
import com.example.feature_character_detail.di.CharacterDetailComponentHolder
import com.example.feature_character_detail.domain.model.Character

class CharacterDetailFragment : Fragment() {

    companion object {
        private const val CHARACTER_ID_KEY = "characterId"
    }

    private lateinit var binding: FragmentCharacterDetailBinding

    private val viewModel: DetailViewModel by viewModels {
        CharacterDetailComponentHolder.getComponent().viewModelsFactory()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCharacterDetailBinding.inflate(inflater, container, false)
        val characterId = arguments?.getString(CHARACTER_ID_KEY)?.toInt()

        if (characterId != null) {
            viewModel.getCharacter(characterId)
            viewModel.character.observe(viewLifecycleOwner) {
                if (it != null) {
                    bindCharacter(it)
                }
            }
        }
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        CharacterDetailComponentHolder.getComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (requireActivity() as AppCompatActivity).onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            (requireActivity() as ToFlowNavigatable).navigateToFlow(NavigationFlow.CharactersListFlow)
        }
    }

    private fun bindCharacter(character: Character) {
        binding.characterName.text = getString(R.string.character_name, character.name)
        binding.characterGender.text = getString(R.string.character_gender, character.gender)
        binding.characterCreated.text = getString(R.string.character_created, character.created.substring(0, 10))
        binding.characterSpecies.text = getString(R.string.characters_species, character.species)
        binding.characterStatus.text = getString(R.string.character_status, character.status)
        Glide.with(requireContext()).load(character.image).into(binding.characterImage)
        requireActivity().title = character.name
    }
}