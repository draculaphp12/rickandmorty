package com.example.feature_character_detail.presentation.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_character_detail.domain.model.Character
import com.example.feature_character_detail.domain.usecase.GetCharacterUseCase
import com.example.feature_character_detail.domain.usecase.LoadCharacterFromDbUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val getCharacterUseCase: GetCharacterUseCase,
    private val loadCharacterFromDbUseCase: LoadCharacterFromDbUseCase
) : ViewModel() {

    private val _character: MutableLiveData<Character?> = MutableLiveData()
    val character: LiveData<Character?>
        get() = _character

    fun getCharacter(characterId: Int) {
        viewModelScope.launch {
            try {
                _character.value = getCharacterUseCase.execute(characterId)
            } catch (e: Exception) {
                _character.value = loadCharacterFromDbUseCase.execute(characterId)
            }
        }
    }
}