package com.example.feature_character_detail.data

import com.example.core_db_api.data.DbClientApi
import com.example.core_network_api.data.HttpClientApi
import com.example.feature_character_detail.domain.CharacterRepository
import com.example.feature_character_detail.domain.model.Character
import javax.inject.Inject

class CharacterRepositoryImpl @Inject constructor(
    private val httpClientApi: HttpClientApi,
    private val dbClientApi: DbClientApi
) : CharacterRepository {
    override suspend fun getCharacter(characterId: Int): Character {
        return httpClientApi.getCharacter(characterId).toCharacter()
    }

    override suspend fun loadCharacterFromDb(characterId: Int): Character? {
        return dbClientApi.charactersRepository.getById(characterId)?.toCharacter()
    }
}