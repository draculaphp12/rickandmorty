package com.example.feature_character_detail.data

import com.example.core_db_api.data.models.CharacterDb
import com.example.core_network_api.data.CharacterResponse
import com.example.feature_character_detail.domain.model.Character

fun CharacterDb.toCharacter(): Character = Character(
    id, name, gender, status, species, created, image
)

fun CharacterResponse.toCharacter(): Character = Character(
    id, name, gender, status, species, created, image
)

fun Character.toDbCharacter(): CharacterDb = CharacterDb(
    id, name, gender, status, species, created, image
)