package com.example.feature_character_detail.domain

import com.example.feature_character_detail.domain.model.Character

interface CharacterRepository {
    suspend fun getCharacter(characterId: Int): Character

    suspend fun loadCharacterFromDb(characterId: Int): Character?
}