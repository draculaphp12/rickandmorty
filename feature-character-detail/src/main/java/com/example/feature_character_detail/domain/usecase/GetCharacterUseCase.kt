package com.example.feature_character_detail.domain.usecase

import com.example.feature_character_detail.domain.CharacterRepository
import com.example.feature_character_detail.domain.model.Character
import javax.inject.Inject

class GetCharacterUseCase @Inject constructor(
    private val characterRepository: CharacterRepository
) {
    suspend fun execute(characterId: Int): Character {
        return characterRepository.getCharacter(characterId)
    }
}