package com.example.feature_character_detail.di

import com.example.feature_character_detail.data.CharacterRepositoryImpl
import com.example.feature_character_detail.domain.CharacterRepository
import dagger.Binds
import dagger.Module

@Module
internal abstract class CharacterDetailModule {
    @Binds
    abstract fun provideCharactersRepository(charactersRepositoryImpl: CharacterRepositoryImpl): CharacterRepository
}