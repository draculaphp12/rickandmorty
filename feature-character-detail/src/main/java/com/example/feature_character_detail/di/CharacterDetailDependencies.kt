package com.example.feature_character_detail.di

import android.content.Context
import com.example.core_db_api.data.DbClientApi
import com.example.core_network_api.data.HttpClientApi
import com.example.module_injector.BaseDependencies

interface CharacterDetailDependencies : BaseDependencies {
    fun httpClient(): HttpClientApi

    fun dbClient(): DbClientApi

    fun context(): Context
}