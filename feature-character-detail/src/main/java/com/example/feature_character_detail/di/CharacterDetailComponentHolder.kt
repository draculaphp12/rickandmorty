package com.example.feature_character_detail.di

import android.util.Log
import com.example.module_injector.ComponentHolder

object CharacterDetailComponentHolder : ComponentHolder<CharacterDetailFeatureApi, CharacterDetailDependencies> {
    private var characterDetailFeatureComponent: CharacterDetailComponent? = null

    override fun init(dependencies: CharacterDetailDependencies) {
        Log.i("CharactersDetailCH", "init()")
        if (characterDetailFeatureComponent == null) {
            synchronized(CharacterDetailComponentHolder::class.java) {
                if (characterDetailFeatureComponent == null) {
                    Log.i("CharacterDetailCH", "initAndGet()")
                    characterDetailFeatureComponent = CharacterDetailComponent.initAndGet(dependencies)
                }
            }
        }
    }

    override fun get(): CharacterDetailComponent = getComponent()

    internal fun getComponent(): CharacterDetailComponent {
        checkNotNull(characterDetailFeatureComponent) { "CharacterDetailComponent was not initialized!" }
        return characterDetailFeatureComponent!!
    }

    override fun reset() {
        Log.i("CharacterDetailCH", "reset()")
        characterDetailFeatureComponent = null
    }
}