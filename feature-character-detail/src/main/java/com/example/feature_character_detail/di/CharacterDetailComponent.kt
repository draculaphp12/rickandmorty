package com.example.feature_character_detail.di

import com.example.feature_character_detail.presentation.view.CharacterDetailFragment
import com.example.feature_character_detail.presentation.view.CharacterDetailViewModelFactory
import dagger.Component

@Component(
    dependencies = [CharacterDetailDependencies::class],
    modules = [CharacterDetailModule::class]
)
abstract class CharacterDetailComponent : CharacterDetailFeatureApi {
    internal abstract fun inject(fragment: CharacterDetailFragment)

    companion object {
        fun initAndGet(charactersListFeatureDependencies: CharacterDetailDependencies): CharacterDetailComponent {
            return DaggerCharacterDetailComponent.builder()
                .characterDetailDependencies(charactersListFeatureDependencies)
                .build()
        }
    }

    abstract fun viewModelsFactory(): CharacterDetailViewModelFactory
}