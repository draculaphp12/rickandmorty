package com.example.feature_characters_list.presentation.view

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_characters_list.R
import com.example.feature_characters_list.databinding.FragmentCharactersListBinding
import com.example.feature_characters_list.di.CharactersListComponentHolder
import com.example.feature_characters_list.presentation.CharactersAdapter
import com.example.feature_characters_list.presentation.DebouncingQueryTextListener
import com.example.feature_characters_list.presentation.DefaultLoadStateAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class CharactersListFragment : Fragment() {
    
    @Inject
    lateinit var charactersAdapter: CharactersAdapter

    @Inject
    lateinit var loadStateAdapter: DefaultLoadStateAdapter

    private lateinit var mainLoadStateHolder: DefaultLoadStateAdapter.Holder

    @Inject
    lateinit var layoutManager: LinearLayoutManager

    private lateinit var binding: FragmentCharactersListBinding

    private val viewModel: CharactersViewModel by viewModels {
        CharactersListComponentHolder.getComponent().viewModelsFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        CharactersListComponentHolder.getComponent().inject(this)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        val searchItem: MenuItem = menu.findItem(R.id.search)
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.setOnQueryTextListener(
            DebouncingQueryTextListener { newText ->
                newText?.let {
                    if (it.isEmpty()) {
                        viewModel.resetSearch()
                    } else {
                        viewModel.setSearch(newText)
                    }
                }
            }
        )

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        requireActivity().title = getString(R.string.characters_list_title)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        binding = FragmentCharactersListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setList()
    }

    private fun setList() {
        binding.charactersList.adapter = charactersAdapter.withLoadStateFooter(loadStateAdapter)
        binding.charactersList.layoutManager = layoutManager
        binding.charactersList.addItemDecoration(
            DividerItemDecoration(binding.charactersList.context, DividerItemDecoration.VERTICAL)
        )

        mainLoadStateHolder = DefaultLoadStateAdapter.Holder(
            binding
        )

        observeCharacters()
        observeLoadState()
    }

    private fun observeLoadState() {
        lifecycleScope.launch {
            charactersAdapter.loadStateFlow.collectLatest { state ->
                mainLoadStateHolder.bind(state.refresh)
            }
        }
    }

    private fun observeCharacters() {
        lifecycleScope.launch {
            viewModel.charactersFlow.collectLatest {
                charactersAdapter.submitData(it)
            }
        }
    }
}