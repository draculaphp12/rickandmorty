package com.example.feature_characters_list.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_characters_list.databinding.FragmentCharactersListBinding

class DefaultLoadStateAdapter : LoadStateAdapter<DefaultLoadStateAdapter.Holder>() {

    override fun onBindViewHolder(holder: Holder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): Holder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = FragmentCharactersListBinding.inflate(inflater, parent, false)
        return Holder(binding)
    }

    class Holder(
        private val binding: FragmentCharactersListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(loadState: LoadState) = with(binding) {
            if (loadState is LoadState.Error) {
                Toast.makeText(itemView.context, loadState.error.localizedMessage, Toast.LENGTH_SHORT).show()
            }
            progressBar.isVisible = loadState is LoadState.Loading
        }
    }

}