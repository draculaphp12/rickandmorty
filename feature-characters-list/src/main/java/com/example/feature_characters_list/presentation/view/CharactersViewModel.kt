package com.example.feature_characters_list.presentation.view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.feature_characters_list.domain.CharactersPagingRepository
import com.example.feature_characters_list.domain.model.Character
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import javax.inject.Inject

@ExperimentalCoroutinesApi
class CharactersViewModel @Inject constructor(
    charactersPagingRepository: CharactersPagingRepository
) : ViewModel() {
    val charactersFlow: Flow<PagingData<Character>>

    private val search =  MutableLiveData("")

    init {
        charactersFlow = search
            .asFlow()
            .flatMapLatest {
                charactersPagingRepository.getPagedCharacters(it)
            }
            .cachedIn(viewModelScope)
    }

    fun setSearch(search: String) {
        this.search.value = search
    }

    fun resetSearch() {
        this.search.value = ""
    }
}