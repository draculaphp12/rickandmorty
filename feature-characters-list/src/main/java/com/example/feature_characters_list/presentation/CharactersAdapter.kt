package com.example.feature_characters_list.presentation

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.core_navigation.NavigationFlow
import com.example.core_navigation.ToFlowNavigatable
import com.example.feature_characters_list.domain.model.Character
import com.example.feature_characters_list.databinding.CharacterItemBinding
import com.example.feature_characters_list.presentation.view.CharactersListFragment

class CharactersAdapter : PagingDataAdapter<Character, CharactersAdapter.CharactersViewHolder>(CharactersDiffCallback()) {

    inner class CharactersViewHolder(private val itemBinding: CharacterItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(character: Character) {
            itemBinding.characterName.text = character.name
            Glide.with(itemView.context)
                .load(character.image)
                .circleCrop()
                .into(itemBinding.characterImage)
            itemView.setOnClickListener {
                (it.context as ToFlowNavigatable).navigateToLink(
                    Uri.parse("rickandmorty://characters/${character.id}")
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharactersViewHolder {
        val rootView = CharacterItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CharactersViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: CharactersViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }
}

class CharactersDiffCallback : DiffUtil.ItemCallback<Character>() {
    override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem == newItem
    }
}