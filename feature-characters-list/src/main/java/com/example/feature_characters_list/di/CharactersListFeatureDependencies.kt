package com.example.feature_characters_list.di

import android.content.Context
import com.example.core_db_api.data.DbClientApi
import com.example.core_network_api.data.HttpClientApi
import com.example.module_injector.BaseDependencies


interface CharactersListFeatureDependencies : BaseDependencies{
    fun httpClient(): HttpClientApi

    fun dbClient(): DbClientApi

    fun context(): Context
}