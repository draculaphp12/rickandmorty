package com.example.feature_characters_list.di

import android.util.Log
import com.example.module_injector.ComponentHolder

object CharactersListComponentHolder : ComponentHolder<CharactersListFeatureApi, CharactersListFeatureDependencies> {

    private var charactersListFeatureComponent: CharactersListFeatureComponent? = null

    override fun init(dependencies: CharactersListFeatureDependencies) {
        Log.i("CharactersListFeatureCH", "init()")
        if (charactersListFeatureComponent == null) {
            synchronized(CharactersListComponentHolder::class.java) {
                if (charactersListFeatureComponent == null) {
                    Log.i("CharactersListFeatureCH", "initAndGet()")
                    charactersListFeatureComponent = CharactersListFeatureComponent.initAndGet(dependencies)
                }
            }
        }
    }

   override fun get(): CharactersListFeatureComponent = getComponent()

   internal fun getComponent(): CharactersListFeatureComponent {
        checkNotNull(charactersListFeatureComponent) { "CharactersListFeatureComponent was not initialized!" }
        return charactersListFeatureComponent!!
    }

    override fun reset() {
        Log.i("CharactersListFeatureCH", "reset()")
        charactersListFeatureComponent = null
    }

}