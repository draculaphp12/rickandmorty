package com.example.feature_characters_list.di

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_characters_list.data.CharactersRepositoryImpl
import com.example.feature_characters_list.domain.CharactersRepository
import com.example.feature_characters_list.presentation.CharactersAdapter
import com.example.feature_characters_list.presentation.DefaultLoadStateAdapter
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
internal abstract class CharactersListFeatureModule {
    @Binds
    abstract fun provideCharactersRepository(charactersRepositoryImpl: CharactersRepositoryImpl): CharactersRepository

    companion object {
        @Provides
        fun provideCharactersAdapter(): CharactersAdapter = CharactersAdapter()

        @Provides
        fun provideDefaultLoadStateAdapter(): DefaultLoadStateAdapter = DefaultLoadStateAdapter()

        @Provides
        fun provideLinearLayoutManager(context: Context): LinearLayoutManager = LinearLayoutManager(context)
    }
}