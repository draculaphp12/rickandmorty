package com.example.feature_characters_list.di

import com.example.feature_characters_list.presentation.view.CharactersListFragment
import com.example.feature_characters_list.presentation.view.CharactersListViewModelFactory
import dagger.Component


@Component(
    dependencies = [CharactersListFeatureDependencies::class],
    modules = [CharactersListFeatureModule::class]
)
abstract class CharactersListFeatureComponent : CharactersListFeatureApi {

    internal abstract fun inject(fragment: CharactersListFragment)

    companion object {
        fun initAndGet(charactersListFeatureDependencies: CharactersListFeatureDependencies): CharactersListFeatureComponent {
            return DaggerCharactersListFeatureComponent.builder()
                .charactersListFeatureDependencies(charactersListFeatureDependencies)
                .build()
        }
    }

    abstract fun viewModelsFactory(): CharactersListViewModelFactory
}