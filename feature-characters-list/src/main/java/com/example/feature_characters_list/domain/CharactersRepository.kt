package com.example.feature_characters_list.domain

import com.example.feature_characters_list.domain.model.Character

interface CharactersRepository {
    suspend fun getCharacters(page: Int = 1): List<Character>

    suspend fun searchCharacters(name: String): List<Character>

    suspend fun saveCharactersList(characters: List<Character>)

    suspend fun loadCharactersFromDb(limit: Int, offset: Int, searchBy: String = ""): List<Character>?
}