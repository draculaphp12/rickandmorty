package com.example.feature_characters_list.domain

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.feature_characters_list.domain.model.Character
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CharactersPagingRepository @Inject constructor(
    private val charactersPagingSource: CharactersPagingSource
) {
    fun getPagedCharacters(searchString: String? = null): Flow<PagingData<Character>> {
        charactersPagingSource.setSearchString(searchString)
        return Pager(
            config = PagingConfig(
                pageSize = CharactersPagingSource.PAGE_SIZE,
                enablePlaceholders = false,
                initialLoadSize = CharactersPagingSource.PAGE_SIZE
            ),
            pagingSourceFactory = { charactersPagingSource }
        ).flow
    }
}