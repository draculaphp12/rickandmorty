package com.example.feature_characters_list.domain.model

import java.io.Serializable

data class Character(
    val id: Int,
    val name: String,
    val gender: String,
    val status: String,
    val species: String,
    val created: String,
    val image: String
) : Serializable
