package com.example.feature_characters_list.domain

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.feature_characters_list.domain.model.Character
import javax.inject.Inject

class CharactersPagingSource @Inject constructor(
    private val charactersRepository: CharactersRepository
) : PagingSource<Int, Character>() {

    companion object {
        const val START_PAGE = 1
        const val PAGE_SIZE = 20
    }

    private var searchString: String? = null

    fun setSearchString(searchString: String?) {
        this.searchString = searchString
    }

    override fun getRefreshKey(state: PagingState<Int, Character>): Int {
        return START_PAGE
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Character> {
        val pageIndex = params.key ?: START_PAGE
        return try {
            val characters: List<Character> = try {
                if (searchString.isNullOrEmpty()) {
                    val characters = charactersRepository.getCharacters(pageIndex)
                    charactersRepository.saveCharactersList(characters)
                    characters
                } else {
                    charactersRepository.searchCharacters(searchString!!)
                }
            } catch (e: Exception) {
                if (searchString.isNullOrEmpty()) {
                    charactersRepository.loadCharactersFromDb(
                        PAGE_SIZE,
                        PAGE_SIZE * (pageIndex - 1)
                    ) ?: listOf()
                } else {
                    charactersRepository.loadCharactersFromDb(
                        PAGE_SIZE,
                        PAGE_SIZE * (pageIndex - 1),
                        searchString!!
                    ) ?: listOf()
                }
            }
            LoadResult.Page(
                data = characters,
                prevKey = if (pageIndex == START_PAGE) null else pageIndex - 1,
                nextKey = if (characters.size == PAGE_SIZE) pageIndex + 1 else null
            )
        } catch (e: Exception) {
            LoadResult.Error(
                throwable = e
            )
        }
    }
}