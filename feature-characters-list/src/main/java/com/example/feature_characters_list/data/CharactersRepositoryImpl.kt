package com.example.feature_characters_list.data

import com.example.core_db_api.data.DbClientApi
import com.example.core_network_api.data.HttpClientApi
import com.example.feature_characters_list.domain.CharactersRepository
import com.example.feature_characters_list.domain.model.Character
import javax.inject.Inject

class CharactersRepositoryImpl @Inject constructor(
    private val httpClient: HttpClientApi,
    private val dbClient: DbClientApi
) : CharactersRepository {
    override suspend fun getCharacters(page: Int): List<Character> {
        return httpClient.getCharacters(page).map { it.toCharacter() }
    }

    override suspend fun searchCharacters(name: String): List<Character> {
        return httpClient.searchCharacters(name).map { it.toCharacter() }
    }

    override suspend fun saveCharactersList(characters: List<Character>) {
        dbClient.charactersRepository.saveAll(characters.map { it.toDbCharacter() })
    }

    override suspend fun loadCharactersFromDb(limit: Int, offset: Int, searchBy: String): List<Character>? {
        return dbClient.charactersRepository.getAll(limit, offset, searchBy)?.map { it.toCharacter() }
    }

}