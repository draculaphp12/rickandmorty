package com.example.rickandmorty

import android.app.Application
import android.content.Context
import com.example.core_db_api.data.DbClientApi
import com.example.core_db_impl.di.CoreDbComponent
import com.example.core_network_api.data.HttpClientApi
import com.example.core_network_impl.di.CoreNetworkComponent
import com.example.feature_character_detail.di.CharacterDetailComponentHolder
import com.example.feature_character_detail.di.CharacterDetailDependencies
import com.example.feature_characters_list.di.CharactersListComponentHolder
import com.example.feature_characters_list.di.CharactersListFeatureComponent
import com.example.feature_characters_list.di.CharactersListFeatureDependencies
import com.example.rickandmorty.di.AppComponent
import com.example.rickandmorty.di.DaggerAppComponent

class DaggerArchApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        AppComponent.init(
            DaggerAppComponent.builder()
                .build()
        )

        AppComponent.get().inject(this)
        CharactersListComponentHolder.init(object : CharactersListFeatureDependencies {
            override fun httpClient(): HttpClientApi {
                return CoreNetworkComponent.get().httpClientApi()
            }

            override fun dbClient(): DbClientApi {
                val dbClient = CoreDbComponent.get().dbClientApi()
                dbClient.init(appContext)
                return dbClient
            }

            override fun context(): Context = appContext
        })
        CharacterDetailComponentHolder.init(object : CharacterDetailDependencies {
            override fun httpClient(): HttpClientApi {
                return CoreNetworkComponent.get().httpClientApi()
            }

            override fun dbClient(): DbClientApi {
                val dbClient = CoreDbComponent.get().dbClientApi()
                dbClient.init(appContext)
                return dbClient
            }

            override fun context(): Context = appContext
        })
    }

    companion object {
        @Volatile
        lateinit var appContext: Context
            private set
    }
}