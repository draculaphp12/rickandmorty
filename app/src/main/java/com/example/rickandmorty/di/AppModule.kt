package com.example.rickandmorty.di

import android.content.Context
import com.example.core_db_api.data.DbClientApi
import com.example.core_db_impl.di.CoreDbComponent
import com.example.core_network_api.data.HttpClientApi
import com.example.core_network_impl.di.CoreNetworkComponent
import com.example.feature_characters_list.di.CharactersListComponentHolder
import com.example.feature_characters_list.di.CharactersListFeatureApi
import com.example.feature_characters_list.di.CharactersListFeatureComponent
import com.example.feature_characters_list.di.CharactersListFeatureDependencies
import com.example.rickandmorty.DaggerArchApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun provideContext(): Context {
        return DaggerArchApplication.appContext
    }

    @Singleton
    @Provides
    fun provideCharactersListFeatureDependencies(): CharactersListFeatureDependencies {
        return object : CharactersListFeatureDependencies {
            override fun httpClient(): HttpClientApi = CoreNetworkComponent.get().httpClientApi()
            override fun dbClient(): DbClientApi = CoreDbComponent.get().dbClientApi()
            override fun context(): Context = DaggerArchApplication.appContext
        }
    }

    @Provides
    fun provideFeatureCharactersList(dependencies: CharactersListFeatureDependencies): CharactersListFeatureComponent {
        CharactersListComponentHolder.init(dependencies)
        return CharactersListComponentHolder.get()
    }
}